#include "pkcs11.h"

extern CK_FUNCTION_LIST function_list;

CK_RV C_Initialize(CK_VOID_PTR pInitArgs){

	//check if application supports multithreading => pInitArgs must be NULL
	if (pInitArgs != NULL_PTR)
		return CKR_CANT_LOCK;



	return CKR_OK;
}

CK_RV C_Finalize(CK_VOID_PTR pReserved){
	if (pReserved != NULL_PTR)
		return CKR_ARGUMENTS_BAD;
	return CKR_OK;
}

CK_RV C_GetFunctionList(CK_FUNCTION_LIST_PTR_PTR ppFunctionList){

	if (ppFunctionList == NULL_PTR)
		return CKR_ARGUMENTS_BAD;

	*ppFunctionList = &function_list;
	return CKR_OK;
}

CK_RV C_GetInfo(CK_INFO_PTR pInfo){
	return CKR_OK;
}

int main()
{

	return 0;
}

CK_RV C_InitToken(CK_SLOT_ID      slotID,
				CK_UTF8CHAR_PTR pPin,
				CK_ULONG        ulPinLen,
				CK_UTF8CHAR_PTR pLabel){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_InitPIN(CK_SESSION_HANDLE hSession,
				CK_UTF8CHAR_PTR   pPin,
				CK_ULONG          ulPinLen){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_SetPIN(CK_SESSION_HANDLE hSession,
				CK_UTF8CHAR_PTR   pOldPin,
				CK_ULONG          ulOldLen,
				CK_UTF8CHAR_PTR   pNewPin,
				CK_ULONG          ulNewLen){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_GetOperationState(CK_SESSION_HANDLE hSession,
						CK_BYTE_PTR       pOperationState,
						CK_ULONG_PTR      pulOperationStateLen){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_SetOperationState(CK_SESSION_HANDLE hSession,
						CK_BYTE_PTR      pOperationState,
						CK_ULONG         ulOperationStateLen,
						CK_OBJECT_HANDLE hEncryptionKey,
						CK_OBJECT_HANDLE hAuthenticationKey){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_CreateObject(CK_SESSION_HANDLE hSession,
					CK_ATTRIBUTE_PTR  pTemplate,
					CK_ULONG          ulCount,
					CK_OBJECT_HANDLE_PTR phObject){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_CopyObject(CK_SESSION_HANDLE    hSession,
					CK_OBJECT_HANDLE     hObject,
					CK_ATTRIBUTE_PTR     pTemplate,
					CK_ULONG             ulCount,
					CK_OBJECT_HANDLE_PTR phNewObject){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_DestroyObject(CK_SESSION_HANDLE hSession,
					CK_OBJECT_HANDLE  hObject){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_GetObjectSize(CK_SESSION_HANDLE hSession,
					CK_OBJECT_HANDLE  hObject,
					CK_ULONG_PTR      pulSize){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_SetAttributeValue(CK_SESSION_HANDLE hSession,
						CK_OBJECT_HANDLE  hObject,
						CK_ATTRIBUTE_PTR  pTemplate,
						CK_ULONG          ulCount){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_DigestKey(CK_SESSION_HANDLE hSession,
					CK_OBJECT_HANDLE  hKey){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_SignRecoverInit(CK_SESSION_HANDLE hSession,
						CK_MECHANISM_PTR  pMechanism,
						CK_OBJECT_HANDLE  hKey){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_SignRecover(CK_SESSION_HANDLE hSession,
					CK_BYTE_PTR       pData,
					CK_ULONG          ulDataLen,
					CK_BYTE_PTR       pSignature,
					CK_ULONG_PTR      pulSignatureLen){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_VerifyInit(CK_SESSION_HANDLE hSession,
					CK_MECHANISM_PTR  pMechanism,
					CK_OBJECT_HANDLE  hKey){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_Verify(CK_SESSION_HANDLE hSession,
				CK_BYTE_PTR       pData,
				CK_ULONG          ulDataLen,
				CK_BYTE_PTR       pSignature,
				CK_ULONG          ulSignatureLen){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_VerifyUpdate(CK_SESSION_HANDLE hSession,
					CK_BYTE_PTR       pPart,
					CK_ULONG          ulPartLen){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_VerifyFinal(CK_SESSION_HANDLE hSession,
					CK_BYTE_PTR       pSignature,
					CK_ULONG          ulSignatureLen){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_VerifyRecoverInit(CK_SESSION_HANDLE hSession,
						CK_MECHANISM_PTR  pMechanism,
						CK_OBJECT_HANDLE  hKey){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_VerifyRecover(CK_SESSION_HANDLE hSession,
					CK_BYTE_PTR       pSignature,
					CK_ULONG          ulSignatureLen,
					CK_BYTE_PTR       pData,
					CK_ULONG_PTR      pulDataLen){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_DigestEncryptUpdate(CK_SESSION_HANDLE hSession,
							CK_BYTE_PTR       pPart,
							CK_ULONG          ulPartLen,
							CK_BYTE_PTR       pEncryptedPart,
							CK_ULONG_PTR      pulEncryptedPartLen){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_DecryptDigestUpdate(CK_SESSION_HANDLE hSession,
							CK_BYTE_PTR       pEncryptedPart,
							CK_ULONG          ulEncryptedPartLen,
							CK_BYTE_PTR       pPart,
							CK_ULONG_PTR      pulPartLen){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_SignEncryptUpdate(CK_SESSION_HANDLE hSession,
						CK_BYTE_PTR       pPart,
						CK_ULONG          ulPartLen,
						CK_BYTE_PTR       pEncryptedPart,
						CK_ULONG_PTR      pulEncryptedPartLen){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_DecryptVerifyUpdate(CK_SESSION_HANDLE hSession,
							CK_BYTE_PTR       pEncryptedPart,
							CK_ULONG          ulEncryptedPartLen,
							CK_BYTE_PTR       pPart,
							CK_ULONG_PTR      pulPartLen){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_GenerateKey(CK_SESSION_HANDLE    hSession,
					CK_MECHANISM_PTR     pMechanism,
					CK_ATTRIBUTE_PTR     pTemplate,
					CK_ULONG             ulCount,
					CK_OBJECT_HANDLE_PTR phKey){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_GenerateKeyPair(CK_SESSION_HANDLE    hSession,
						CK_MECHANISM_PTR     pMechanism,
						CK_ATTRIBUTE_PTR     pPublicKeyTemplate,
						CK_ULONG             ulPublicKeyAttributeCount,
						CK_ATTRIBUTE_PTR     pPrivateKeyTemplate,
						CK_ULONG             ulPrivateKeyAttributeCount,
						CK_OBJECT_HANDLE_PTR phPublicKey,
						CK_OBJECT_HANDLE_PTR phPrivateKey){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_WrapKey(CK_SESSION_HANDLE hSession,
				CK_MECHANISM_PTR  pMechanism,
				CK_OBJECT_HANDLE  hWrappingKey,
				CK_OBJECT_HANDLE  hKey,
				CK_BYTE_PTR       pWrappedKey,
				CK_ULONG_PTR      pulWrappedKeyLen){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_UnwrapKey(CK_SESSION_HANDLE    hSession,
				CK_MECHANISM_PTR     pMechanism,
				CK_OBJECT_HANDLE     hUnwrappingKey,
				CK_BYTE_PTR          pWrappedKey,
				CK_ULONG             ulWrappedKeyLen,
				CK_ATTRIBUTE_PTR     pTemplate,
				CK_ULONG             ulAttributeCount,
				CK_OBJECT_HANDLE_PTR phKey){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_DeriveKey(CK_SESSION_HANDLE    hSession,
				CK_MECHANISM_PTR     pMechanism,
				CK_OBJECT_HANDLE     hBaseKey,
				CK_ATTRIBUTE_PTR     pTemplate,
				CK_ULONG             ulAttributeCount,
				CK_OBJECT_HANDLE_PTR phKey){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_SeedRandom(CK_SESSION_HANDLE hSession,
				CK_BYTE_PTR       pSeed,
				CK_ULONG          ulSeedLen){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_GenerateRandom(CK_SESSION_HANDLE hSession,
					CK_BYTE_PTR       RandomData,
					CK_ULONG          ulRandomLen){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_GetFunctionStatus(CK_SESSION_HANDLE hSession){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_CancelFunction(CK_SESSION_HANDLE hSession){
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_RV C_WaitForSlotEvent(CK_FLAGS flags,
						CK_SLOT_ID_PTR pSlot,
						CK_VOID_PTR pRserved){
	return CKR_FUNCTION_NOT_SUPPORTED;
}

CK_FUNCTION_LIST function_list = {
	{2,40},
	C_Initialize,
	C_Finalize,
	C_GetInfo,
	C_GetFunctionList,
	C_GetSlotList,
	C_GetSlotInfo,
	C_GetTokenInfo,
	C_GetMechanismList,
	C_GetMechanismInfo,
	C_InitToken,//NOT_SUPPORTED
	C_InitPIN,//NOT_SUPPORTED
	C_SetPIN,//NOT_SUPPORTED
	C_OpenSession,
	C_CloseSession,
	C_CloseAllSessions,
	C_GetSessionInfo,
	C_GetOperationState,//NOT_SUPPORTED
	C_SetOperationState,//NOT_SUPPORTED
	C_Login,
	C_Logout,
	C_CreateObject,//NOT_SUPPORTED
	C_CopyObject,//NOT_SUPPORTED
	C_DestroyObject,//NOT_SUPPORTED
	C_GetObjectSize,//NOT_SUPPORTED
	C_GetAttributeValue,
	C_SetAttributeValue,//NOT_SUPPORTED
	C_FindObjectsInit,
	C_FindObjects,
	C_FindObjectsFinal,
	C_EncryptInit,
	C_Encrypt,
	C_EncryptUpdate,
	C_EncryptFinal,
	C_DecryptInit,
	C_Decrypt,
	C_DecryptUpdate,
	C_DecryptFinal,
	C_DigestInit,
	C_Digest,
	C_DigestUpdate,
	C_DigestKey,//NOT_SUPPORTED
	C_DigestFinal,
	C_SignInit,
	C_Sign,
	C_SignUpdate,
	C_SignFinal,
	C_SignRecoverInit,//NOT_SUPPORTED
	C_SignRecover,//NOT_SUPPORTED
	C_VerifyInit,//NOT_SUPPORTED
	C_Verify,//NOT_SUPPORTED
	C_VerifyUpdate,//NOT_SUPPORTED
	C_VerifyFinal,//NOT_SUPPORTED
	C_VerifyRecoverInit,//NOT_SUPPORTED
	C_VerifyRecover,//NOT_SUPPORTED
	C_DigestEncryptUpdate,//NOT_SUPPORTED
	C_DecryptDigestUpdate,//NOT_SUPPORTED
	C_SignEncryptUpdate,//NOT_SUPPORTED
	C_DecryptVerifyUpdate,//NOT_SUPPORTED
	C_GenerateKey,//NOT_SUPPORTED
	C_GenerateKeyPair,//NOT_SUPPORTED
	C_WrapKey,//NOT_SUPPORTED
	C_UnwrapKey,//NOT_SUPPORTED
	C_DeriveKey,//NOT_SUPPORTED
	C_SeedRandom,//NOT_SUPPORTED
	C_GenerateRandom,//NOT_SUPPORTED
	C_GetFunctionStatus,//NOT_SUPPORTED
	C_CancelFunction,//NOT_SUPPORTED
	C_WaitForSlotEvent//NOT_SUPPORTED
};