# include <iostream>
# include "pkcs11.h"

#define ATMSLOT 282672

CK_RV C_MakeSlotInfo(CK_SLOT_INFO_PTR slotInfo)
{
	if (sizeof(*slotInfo) == sizeof(CK_SLOT_INFO))
	{
		strcpy((char*)slotInfo->manufacturerID, "ATM@Registred");
		strcpy((char*)slotInfo->slotDescription, "ATM_Slots_Management");
		// #define CKF_TOKEN_PRESENT     0x00000001UL  /* a token is there */
		// #define CKF_REMOVABLE_DEVICE  0x00000002UL  /* removable devices*/
		// #define CKF_HW_SLOT           0x00000004UL  /* hardware slot */

		slotInfo->flags = CKF_TOKEN_PRESENT;

		//pt CK_VERSION
		//versiune		major		minor
		//  1.0			 0x01		 0x00
		//  2.10		 0x02		 0x0a
		//  2.11		 0x02		 0x0b

		slotInfo->hardwareVersion.major = (CK_BYTE) '0x01';
		slotInfo->hardwareVersion.minor = (CK_BYTE) '0x00';

		slotInfo->firmwareVersion.major = (CK_BYTE) '0x01';
		slotInfo->firmwareVersion.minor = (CK_BYTE) '0x00';

		return CKR_OK;
	}
	else
		return CKR_ARGUMENTS_BAD;
}

CK_RV C_MakeTokenInfo(CK_TOKEN_INFO_PTR pInfo)
{
	if (sizeof(*pInfo) == sizeof(CK_TOKEN_INFO))
	{
		strcpy((char*)pInfo->label, "ATM@Registred_Token");
		strcpy((char*)pInfo->manufacturerID, "ID89211122131134321");
		strcpy((char*)pInfo->model, "ATMTOKEN1");
		strcpy((char*)pInfo->serialNumber, "00001122334455");
		// #define CKF_RNG                     0x00000001UL  /* has random # generator */
		// #define CKF_WRITE_PROTECTED         0x00000002UL  /* token is write-protected */
		// #define CKF_LOGIN_REQUIRED          0x00000004UL  /* user must login */
		// #define CKF_USER_PIN_INITIALIZED    0x00000008UL  /* normal user's PIN is set */

		pInfo->flags = CKF_RNG | CKF_LOGIN_REQUIRED | CKF_USER_PIN_INITIALIZED | CKF_DUAL_CRYPTO_OPERATIONS | CKF_TOKEN_INITIALIZED;

		pInfo->ulMaxSessionCount = (CK_ULONG)1;
		pInfo->ulSessionCount = (CK_ULONG)1;

		pInfo->ulMaxRwSessionCount = (CK_ULONG)1;
		pInfo->ulRwSessionCount = (CK_ULONG)1;

		pInfo->ulMaxPinLen = (CK_ULONG)32;
		pInfo->ulMinPinLen = (CK_ULONG)8;

		pInfo->ulTotalPublicMemory = (CK_ULONG)2048;
		pInfo->ulFreePublicMemory = (CK_ULONG)2048;
		pInfo->ulTotalPrivateMemory = (CK_ULONG)2048;
		pInfo->ulFreePrivateMemory = (CK_ULONG)2048;

		pInfo->hardwareVersion.major = (CK_BYTE) '0x01';
		pInfo->hardwareVersion.minor = (CK_BYTE) '0x00';

		pInfo->firmwareVersion.major = (CK_BYTE) '0x01';
		pInfo->firmwareVersion.minor = (CK_BYTE) '0x00';

		strcpy((char*)pInfo->utcTime, "19:54:28");

		return CKR_OK;
	}
	else
		return CKR_ARGUMENTS_BAD;
}

CK_RV C_GetSlotList(CK_BBOOL tokenPresent, CK_SLOT_ID_PTR pSlotList, CK_ULONG_PTR pulCount)
{
	//tokenPresent - indicates whether the list obtained includes only those slots with a token present (CK_TRUE), or all slots (CK_FALSE);
	//pulCount - points to the location that receives the number of slots.
	if (pSlotList == NULL_PTR)
	{
		//return number of slots in pulCount
		

		//aici apelez la Tica sa imi dea numarul de SLOTURI si returnez nr cu CKR_OK;
		switch (tokenPresent)
		{
		case true: //daca cauta doar sloturile cu token prezent...
			if (true)//daca a mers apelul la tica
			{
				*pulCount = 1;//adica slotul nostru
				return CKR_OK;
			}
			else
			{
				return CKR_FUNCTION_FAILED;
			}

			break;
		case false://sau toate.. ii e indiferent
			if (true)//daca a mers apelul la tica
			{
				*pulCount = 1;
				return CKR_OK;
			}
			else
			{
				return CKR_FUNCTION_FAILED;
			}

			break;
		}
		
	}
	else
	{
		
		//aici am pus cu Test la final sa mearga momentan
		if(sizeof(*pSlotList) >= sizeof(CK_SLOT_ID))
		{
			//daca lista este suficient de larga sa contina toate sloturile 
			//cer marimea de sloturi al tica
			//returnam lista in pSlotList si dam CKR_OK
			if(tokenPresent)
			{
				*(pSlotList + 0) = (CK_ULONG)ATMSLOT;
			}
			else
			{
				*(pSlotList + 0) = (CK_ULONG)ATMSLOT;
			}

			
			return CKR_OK;
		}
		else //marimea nu este corecta.. 
		{
			//trebuie sa setam pulCount dupa ce o cer de la TIKI TACA
			if (tokenPresent)
			{
				*pulCount = 1;
			}
			else
			{
				*pulCount = 1;
			}
			

			//return too_small
			return CKR_BUFFER_TOO_SMALL;
		}
		
		
	}
	
	return CKR_OK;
}

CK_RV C_GetSlotInfo(CK_SLOT_ID slotID, CK_SLOT_INFO_PTR pInfo)
{ // obtine informatii despre un slot particular din sistem

	//slotID - is the ID of the token's slot;
	//pInfo	- points to the location that receives the token information.
	if (slotID != NULL)
	{
		//cer informatii si le pun in pInfo
		if (slotID != 1)
		{
			return CKR_ARGUMENTS_BAD;
		}
		if (C_MakeSlotInfo(pInfo) == CKR_OK)
		{
			return CKR_OK;
		}
		else
		{
			return CKR_FUNCTION_FAILED;
		}

	}
	return CKR_FUNCTION_FAILED;
	
}

CK_RV C_GetTokenInfo(CK_SLOT_ID slotID, CK_TOKEN_INFO_PTR pInfo)
{ //obtine info despre un token particular din sistem


	//slotID	is the ID of the token's slot;
	//pInfo	points to the location that receives the token information.
	if (slotID != NULL)
	{
		//cer informatii despre TOKEN si le pun in pInfo
		//avem 2 cazuri

		//fie exista token pe slot
		//fie nu.. 
		if (slotID == 1)
		{

			if (true) // sa verifice daca are token pe el.. 
			{
				//aici am mai facut eu o verificare sa eliberez memoria in caz ca este nula
				if (pInfo == NULL_PTR)
				{
					return CKR_HOST_MEMORY;
				}
				else
				{
					//conditia este sa fie TOKENUL pe slotul 1...
					if (C_MakeTokenInfo(pInfo) == CKR_OK)
					{
						return CKR_OK;
					}
					else
					{
						return CKR_FUNCTION_FAILED;
					}
				}
				
			}
			else
				return CKR_TOKEN_NOT_PRESENT;
		}
		else
			return CKR_ARGUMENTS_BAD;
		
		
	}
	return CKR_FUNCTION_FAILED; //asta daca primesc NULL la slot
}

CK_RV C_GetMechanismList(CK_SLOT_ID slotID, CK_MECHANISM_TYPE_PTR pMechanismList, CK_ULONG_PTR pulCount)
{//folosit pentru a obtine o lista de mecanisme suportate de un token

	//SlotID	is the ID of the token's slot;
	//pulCount	points to the location that receives the number of mechanisms.
	if (pMechanismList == NULL_PTR)
	{
		//daca lista e nula, returnam in pulCount numarul de mecanisme, fara a returna o lista
		
		//apelez TICA TICA TICA
		if (true) //adica apelul la tica a reusit
		{
			//nu mai are rost sa fac free
			
			*pulCount = 10;
			return CKR_OK;
		}
		else
		{
			return CKR_FUNCTION_FAILED;
		}
		

	}
	else
	{
		CK_MECHANISM_TYPE_PTR testList = (CK_MECHANISM_TYPE_PTR)malloc(10 * sizeof(CK_MECHANISM_TYPE));
		//aici verific daca coincid valorile primite de la tica
		//sa fie bufferul destul de mare
		CK_ULONG testVal = 10;

		if (sizeof(*testList) >= testVal* sizeof(CK_MECHANISM_TYPE))
		{
			//apelez TICA si obtin lista de lista de mechanisme
			pMechanismList = testList;


			//plus, trebuie sa setam pulCount
			

			
			//apel TICA pt marimea corecta
			*pulCount = 10;
			//val de teste
		}
		else
		{
			//returnam CKR-buf-to-small
			//plus, trebuie sa setam pulCount
			
			//apel TICA pt marimea corecta
			*pulCount = 10;
			//val de teste
			

			return CKR_BUFFER_TOO_SMALL;
		}
	}

	


	return CKR_OK;
}

CK_RV C_GetMechanismInfo(CK_SLOT_ID slotID, CK_MECHANISM_TYPE type, CK_MECHANISM_INFO_PTR pInfo)
{//obtine informatii despre un mecanism particular, posibil suportat de un token

	//slotID	is the ID of the token's slot;
	//type	is the type of mechanism;
	//pInfo	points to the location that receives the mechanism information.
	if (slotID != NULL)
	{
		//avem un slotID valid
		//testam sa avem si un type valid
		if (type != NULL)
		{
			//avem type valid
			//cerem info la TICA
			if (sizeof(*pInfo) == sizeof(CK_MECHANISM_INFO))
			{
				return CKR_OK;
			}
			else
			{
				return CKR_ARGUMENTS_BAD;
			}
			
           //verifica marime pinfo
		}
		else
			return CKR_ARGUMENTS_BAD;
	}
	else
		return CKR_ARGUMENTS_BAD;



}

int main()
{
	CK_ULONG ulSlotCount, ulSlotWithTokenCount;
	CK_SLOT_ID_PTR pSlotList, pSlotWithTokenList;
	CK_RV rv;
	/* Get list of all slots */
	rv = C_GetSlotList(FALSE, NULL_PTR, &ulSlotCount);
	if (rv == CKR_OK) {
		printf("e ok\n");
		printf("%u", ulSlotCount);
		pSlotList = (CK_SLOT_ID_PTR)malloc(ulSlotCount * sizeof(CK_SLOT_ID));
		rv = C_GetSlotList(FALSE, pSlotList, &ulSlotCount);
		if (rv == CKR_OK) {
			/* Now use that list of all slots */
			printf("e ok\n");
		}
		free(pSlotList);
	}
	else
	{
		printf("ceva nu e ok\n");
	}


	CK_UTF8CHAR   slotDescription[64];
	
	char* slotD = "ATM@Registred";

	/*for (int i = 0; i < strlen(slotD); i++)
	{
		*(slotDescription+i) = (CK_UTF8CHAR) slotD[i];
	}
	*(slotDescription + (int)strlen(slotD)) = (CK_UTF8CHAR)'\0';
	printf("%s\n", slotDescription);
	*/

	strcpy((char*)slotDescription, "sdadasdasd|");

	printf("%s\n", slotDescription);


	CK_SLOT_INFO info;
	C_MakeSlotInfo(&info);
	CK_TOKEN_INFO info2;
	C_MakeTokenInfo(&info2);
	return 0;

}